# fsmk_statement
Thought process in the flow : 

1. Important points from FSMI's statement after the judgement
2. What are our current ground efforts and what impact are we expecting
3. What actions are we planning for in the upcoming days
4. What kind of help would be useful

Estimated time : 3 - 5 min talking 

FSMI & FSMK has always been raising it voice against technological solutionism which disregards the impact of socio-econmical issues and needs. UID(aka Aadhaar) is one such project that 
has lead to massive data leakages , built systems of survillence and has had enormous amounts of exclusions. While UIDAI has been batting for its system never having a breach on their servers
we find it extremely unacceptable that UIDAI has everytime not considered that fact that data has been leaking at erronous levels from its subsidiries of whom UIDAI is responsible for. 

As Evgeny Morozov says, “We must not fixate on what this new arsenal of digital technologies allows us to do without first inquiring what is worth doing.” 